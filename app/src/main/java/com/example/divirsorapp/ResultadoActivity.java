package com.example.divirsorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class ResultadoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        TextView displayResultado = (TextView) findViewById(R.id.textView_resultado);
        String resultado = "erro";

        if(getIntent().hasExtra("resultado")) {
            Locale ptBr = new Locale("pt","BR");
            resultado = NumberFormat.getCurrencyInstance(ptBr).format((getIntent().getDoubleExtra("resultado",-1)));
        }

        displayResultado.setText(resultado);



    }
}