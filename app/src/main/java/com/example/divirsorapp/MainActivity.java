package com.example.divirsorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    Locale ptBr = new Locale("pt","BR");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText inputValor = findViewById(R.id.editTextNumberDecimal_valor);
        EditText inputNumPessoas = findViewById(R.id.editTextNumber_Numero_Pessoas);
        Button calcularButton = findViewById(R.id.button_calcular);


        calcularButton.setOnClickListener(view ->{

                Double valor = Double.parseDouble( inputValor.getText().toString());
                Double numPessoas = Double.parseDouble(inputNumPessoas.getText().toString());
                Double resultado = valor/numPessoas;

                Intent calcularIntent = new Intent(MainActivity.this,ResultadoActivity.class);
                calcularIntent.putExtra("resultado",resultado);

                startActivity(calcularIntent);

        });


    }


}